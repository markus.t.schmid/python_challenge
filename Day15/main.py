import data
def count_money(quarter,dime,nickel,penny):
    return (quarter * data.coins["quarter"] + dime * data.coins["dime"] + nickel * data.coins["nickel"] + penny * data.coins["penny"])

def ressources(c):
   recipe = data.MENU[c]
   if (c != "espresso" and recipe["ingredients"]["water"] < data.resources["water"] and recipe["ingredients"]["milk"] < data.resources["milk"] and recipe["ingredients"]["coffee"] < data.resources["coffee"]) or (c == "espresso" and recipe["ingredients"]["water"] < data.resources["water"] and recipe["ingredients"]["coffee"] < data.resources["coffee"]):
       return True
   else:
       return False

state = True
money = 0
while state:
    choice = input("What would you like? (espresso/latte/cappuccino):")
    if choice == "off":
        state = False
        print(f"Your change is $ {money}")
        print("Thank you for using Coffee Maker 2000!\nHave a nice day")
        exit()
    elif choice == "report":
        print(f"Water: {data.resources['water']}ml\nMilk: {data.resources['milk']}ml\nCoffee: {data.resources['coffee']}g")
    elif choice != ("espresso" or "latte" or "cappuccino"):
        print("Sorry I didn't understand that...")
    else:
        print("Please insert coins")
        quarters = int(input("How many quarters?"))
        dime = int(input("How many dimes?"))
        nickel = int(input("How many nickels?"))
        penny = int(input("How many pennies?"))
        money = round(money + float(count_money(quarters,dime,nickel,penny)),2)
        print(f"Balance :$ {money}")
        if choice == "espresso":
            if money > float(data.MENU[choice]["cost"]) and ressources(choice):
                print("Your beverage is being prepared")
                money -= float(data.MENU[choice]["cost"])
                data.resources["water"] -= data.MENU[choice]["ingredients"]["water"]
                data.resources["coffee"] -= data.MENU[choice]["ingredients"]["coffee"]
                #data.resources["milk"] -= data.MENU[choice]["ingredients"]["milk"] # No milk needed....
                print(f"Remaining Balance $ {round(money,2)}\nHere is your {choice}. Enjoy!")
            else:
                if not ressources(choice):
                    print("Not enough resources! Please refill. ")
                else:
                    print("Balance to low")
        elif choice == "latte":
            if money > float(data.MENU[choice]["cost"]) and ressources(choice):
                print("Your beverage is being prepared")
                money -= float(data.MENU[choice]["cost"])
                data.resources["water"] -= data.MENU[choice]["ingredients"]["water"]
                data.resources["coffee"] -= data.MENU[choice]["ingredients"]["coffee"]
                data.resources["milk"] -= data.MENU[choice]["ingredients"]["milk"]
                print(f"Remaining Balance $ {round(money, 2)}\nHere is your {choice}. Enjoy!")
            else:
                if not ressources(choice):
                    print("Not enough resources! Please refill. ")
                else:
                    print("Balance to low")
        elif choice == "capuccino":
            if money > float(data.MENU[choice]["cost"]) and ressources(choice):
                print("Your beverage is being prepared")
                money -= float(data.MENU[choice]["cost"])
                data.resources["water"] -= data.MENU[choice]["ingredients"]["water"]
                data.resources["coffee"] -= data.MENU[choice]["ingredients"]["coffee"]
                data.resources["milk"] -= data.MENU[choice]["ingredients"]["milk"]
                print(f"Remaining Balance $ {round(money, 2)}\nHere is your {choice}. Enjoy!")
            else:
                if not ressources(choice):
                    print("Not enough resources! Please refill. ")
                else:
                    print("Balance to low")
        elif data.MENU["choice":"cost"] > money:
            print("Balance to low")
        else:
            print("Please choose a different option")

