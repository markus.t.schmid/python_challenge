from turtle import Turtle, Screen
import random
# import colorgram
#
# # Extract 6 colors from an image.
# colors = colorgram.extract('image.jpg', 30)
# list_color=[]
# for color in colors:
#     red = color.rgb.r
#     green = color.rgb.g
#     blue = color.rgb.b
#     rgb_color=(red, green, blue)
#     list_color.append(rgb_color)
# print(list_color)

color_list= [(247, 235, 240), (230, 241, 235), (225, 236, 243), (28, 115, 159), (204, 159, 105), (159, 82, 40), (184, 164, 25), (233, 218, 102), (121, 176, 192), (202, 128, 157), (37, 134, 83), (158, 31, 25), (136, 182, 150), (156, 24, 44), (28, 52, 74), (40, 163, 123), (43, 32, 27), (223, 91, 50), (201, 84, 132), (238, 215, 4), (144, 64, 98), (44, 164, 184), (25, 61, 114), (22, 46, 37), (234, 170, 185), (64, 34, 50), (74, 78, 38), (109, 117, 159), (238, 168, 161)]
tim = Turtle()
tim.shape('turtle')
tim.speed(0)
tim.pensize(10)
tim.hideturtle()
tim.penup()
screen = Screen()
screen.colormode(255)
direction = -1
def draw():
    global direction
    for _ in range(0, 10):
        direction = direction * -1
        for _ in range(0, 10):
            tim.dot(20, random.choice(color_list))
            tim.forward(50)
        tim.left(90*direction)
        tim.forward(50)
        tim.left(90*direction)

draw()
screen.exitonclick()
#
# direction = [0, 90, 180, 270]
#
# # colors = ['snow', 'ghost white', 'white smoke', 'gainsboro', 'floral white', 'old lace',
# #           'linen', 'antique white', 'papaya whip', 'blanched almond', 'bisque', 'peach puff',
# #           'navajo white', 'lemon chiffon', 'mint cream', 'azure', 'alice blue', 'lavender',
# #           'lavender blush', 'misty rose', 'dark slate gray', 'dim gray', 'slate gray',
# #           'light slate gray', 'gray', 'light gray', 'midnight blue', 'navy', 'cornflower blue', 'dark slate blue',
# #           'slate blue', 'medium slate blue', 'light slate blue', 'medium blue', 'royal blue',  'blue',
# #           'dodger blue', 'deep sky blue', 'sky blue', 'light sky blue', 'steel blue', 'light steel blue',
# #           'light blue', 'powder blue', 'pale turquoise', 'dark turquoise', 'medium turquoise', 'turquoise',
# #           'cyan', 'light cyan', 'cadet blue', 'medium aquamarine', 'aquamarine', 'dark green', 'dark olive green',
# #           'dark sea green', 'sea green', 'medium sea green', 'light sea green', 'pale green', 'spring green',
# #           'lawn green', 'medium spring green', 'green yellow', 'lime green', 'yellow green',
# #           'forest green', 'olive drab', 'dark khaki', 'khaki', 'pale goldenrod', 'light goldenrod yellow',
# #           'light yellow', 'yellow', 'gold', 'light goldenrod', 'goldenrod', 'dark goldenrod', 'rosy brown',
# #           'indian red', 'saddle brown', 'sandy brown',
# #           'dark salmon', 'salmon', 'light salmon', 'orange', 'dark orange',
# #           'coral', 'light coral', 'tomato', 'orange red', 'red', 'hot pink', 'deep pink', 'pink', 'light pink',
# #           'pale violet red', 'maroon', 'medium violet red', 'violet red',
# #           'medium orchid', 'dark orchid', 'dark violet', 'blue violet', 'purple', 'medium purple',
# #           'thistle', 'snow2', 'snow3',
# #           'snow4', 'seashell2', 'seashell3', 'seashell4', 'AntiqueWhite1', 'AntiqueWhite2',
# #           'AntiqueWhite3', 'AntiqueWhite4', 'bisque2', 'bisque3', 'bisque4', 'PeachPuff2',
# #           'PeachPuff3', 'PeachPuff4', 'NavajoWhite2', 'NavajoWhite3', 'NavajoWhite4',
# #           'LemonChiffon2', 'LemonChiffon3', 'LemonChiffon4', 'cornsilk2', 'cornsilk3',
# #           'cornsilk4', 'ivory2', 'ivory3', 'ivory4', 'honeydew2', 'honeydew3', 'honeydew4',
# #           'LavenderBlush2', 'LavenderBlush3', 'LavenderBlush4', 'MistyRose2', 'MistyRose3',
# #           'MistyRose4', 'azure2', 'azure3', 'azure4', 'SlateBlue1', 'SlateBlue2', 'SlateBlue3',
# #           'SlateBlue4', 'RoyalBlue1', 'RoyalBlue2', 'RoyalBlue3', 'RoyalBlue4', 'blue2', 'blue4',
# #           'DodgerBlue2', 'DodgerBlue3', 'DodgerBlue4', 'SteelBlue1', 'SteelBlue2',
# #           'SteelBlue3', 'SteelBlue4', 'DeepSkyBlue2', 'DeepSkyBlue3', 'DeepSkyBlue4',
# #           'SkyBlue1', 'SkyBlue2', 'SkyBlue3', 'SkyBlue4', 'LightSkyBlue1', 'LightSkyBlue2',
# #           'LightSkyBlue3', 'LightSkyBlue4', 'Slategray1', 'Slategray2', 'Slategray3',
# #           'Slategray4', 'LightSteelBlue1', 'LightSteelBlue2', 'LightSteelBlue3',
# #           'LightSteelBlue4', 'LightBlue1', 'LightBlue2', 'LightBlue3', 'LightBlue4',
# #           'LightCyan2', 'LightCyan3', 'LightCyan4', 'PaleTurquoise1', 'PaleTurquoise2',
# #           'PaleTurquoise3', 'PaleTurquoise4', 'CadetBlue1', 'CadetBlue2', 'CadetBlue3',
# #           'CadetBlue4', 'turquoise1', 'turquoise2', 'turquoise3', 'turquoise4', 'cyan2', 'cyan3',
# #           'cyan4', 'DarkSlategray1', 'DarkSlategray2', 'DarkSlategray3', 'DarkSlategray4',
# #           'aquamarine2', 'aquamarine4', 'DarkSeaGreen1', 'DarkSeaGreen2', 'DarkSeaGreen3',
# #           'DarkSeaGreen4', 'SeaGreen1', 'SeaGreen2', 'SeaGreen3', 'PaleGreen1', 'PaleGreen2',
# #           'PaleGreen3', 'PaleGreen4', 'SpringGreen2', 'SpringGreen3', 'SpringGreen4',
# #           'green2', 'green3', 'green4', 'chartreuse2', 'chartreuse3', 'chartreuse4',
# #           'OliveDrab1', 'OliveDrab2', 'OliveDrab4', 'DarkOliveGreen1', 'DarkOliveGreen2',
# #           'DarkOliveGreen3', 'DarkOliveGreen4', 'khaki1', 'khaki2', 'khaki3', 'khaki4',
# #           'LightGoldenrod1', 'LightGoldenrod2', 'LightGoldenrod3', 'LightGoldenrod4',
# #           'LightYellow2', 'LightYellow3', 'LightYellow4', 'yellow2', 'yellow3', 'yellow4',
# #           'gold2', 'gold3', 'gold4', 'goldenrod1', 'goldenrod2', 'goldenrod3', 'goldenrod4',
# #           'DarkGoldenrod1', 'DarkGoldenrod2', 'DarkGoldenrod3', 'DarkGoldenrod4',
# #           'RosyBrown1', 'RosyBrown2', 'RosyBrown3', 'RosyBrown4', 'IndianRed1', 'IndianRed2',
# #           'IndianRed3', 'IndianRed4', 'sienna1', 'sienna2', 'sienna3', 'sienna4', 'burlywood1',
# #           'burlywood2', 'burlywood3', 'burlywood4', 'wheat1', 'wheat2', 'wheat3', 'wheat4', 'tan1',
# #           'tan2', 'tan4', 'chocolate1', 'chocolate2', 'chocolate3', 'firebrick1', 'firebrick2',
# #           'firebrick3', 'firebrick4', 'brown1', 'brown2', 'brown3', 'brown4', 'salmon1', 'salmon2',
# #           'salmon3', 'salmon4', 'LightSalmon2', 'LightSalmon3', 'LightSalmon4', 'orange2',
# #           'orange3', 'orange4', 'DarkOrange1', 'DarkOrange2', 'DarkOrange3', 'DarkOrange4',
# #           'coral1', 'coral2', 'coral3', 'coral4', 'tomato2', 'tomato3', 'tomato4', 'OrangeRed2',
# #           'OrangeRed3', 'OrangeRed4', 'red2', 'red3', 'red4', 'DeepPink2', 'DeepPink3', 'DeepPink4',
# #           'HotPink1', 'HotPink2', 'HotPink3', 'HotPink4', 'pink1', 'pink2', 'pink3', 'pink4',
# #           'LightPink1', 'LightPink2', 'LightPink3', 'LightPink4', 'PaleVioletRed1',
# #           'PaleVioletRed2', 'PaleVioletRed3', 'PaleVioletRed4', 'maroon1', 'maroon2',
# #           'maroon3', 'maroon4', 'VioletRed1', 'VioletRed2', 'VioletRed3', 'VioletRed4',
# #           'magenta2', 'magenta3', 'magenta4', 'orchid1', 'orchid2', 'orchid3', 'orchid4', 'plum1',
# #           'plum2', 'plum3', 'plum4', 'MediumOrchid1', 'MediumOrchid2', 'MediumOrchid3',
# #           'MediumOrchid4', 'DarkOrchid1', 'DarkOrchid2', 'DarkOrchid3', 'DarkOrchid4',
# #           'purple1', 'purple2', 'purple3', 'purple4', 'MediumPurple1', 'MediumPurple2',
# #           'MediumPurple3', 'MediumPurple4', 'thistle1', 'thistle2', 'thistle3', 'thistle4',
# #           'grey1', 'grey2', 'grey3', 'grey4', 'grey5', 'grey6', 'grey7', 'grey8', 'grey9', 'grey10',
# #           'grey11', 'grey12', 'grey13', 'grey14', 'grey15', 'grey16', 'grey17', 'grey18', 'grey19',
# #           'grey20', 'grey21', 'grey22', 'grey23', 'grey24', 'grey25', 'grey26', 'grey27', 'grey28',
# #           'grey29', 'grey30', 'grey31', 'grey32', 'grey33', 'grey34', 'grey35', 'grey36', 'grey37',
# #           'grey38', 'grey39', 'grey40', 'grey42', 'grey43', 'grey44', 'grey45', 'grey46', 'grey47',
# #           'grey48', 'grey49', 'grey50', 'grey51', 'grey52', 'grey53', 'grey54', 'grey55', 'grey56',
# #           'grey57', 'grey58', 'grey59', 'grey60', 'grey61', 'grey62', 'grey63', 'grey64', 'grey65',
# #           'grey66', 'grey67', 'grey68', 'grey69', 'grey70', 'grey71', 'grey72', 'grey73', 'grey74',
# #           'grey75', 'grey76', 'grey77', 'grey78', 'grey79', 'grey80', 'grey81', 'grey82', 'grey83',
# #           'grey84', 'grey85', 'grey86', 'grey87', 'grey88', 'grey89', 'grey90', 'grey91', 'grey92',
# #           'grey93', 'grey94', 'grey95', 'grey97', 'grey98', 'grey99']
#
# tim = Turtle()
# tim.shape('turtle')
# tim.speed(0)
# tim.pensize(1)
# screen = Screen()
# screen.colormode(255)
# count = 0
# def draw_shape(sides):
#     angle = 360 / (sides)
#     for _ in range(sides):
#         tim.forward(100)
#         tim.right(angle)
#
# def rand_color():
#     r = random.randint(0, 255)
#     g = random.randint(0, 255)
#     b = random.randint(0, 255)
#     color = (r, g, b)
#     return color
#
# def move():
#     tim.forward(25)
#     tim.setheading(random.choice(direction))
#     tim.pencolor(rand_color())
#
# def circle(gap):
#     for _ in range(int(360/gap)):
#         tim.circle(100)
#         tim.setheading(tim.heading() + gap)
#         tim.pencolor(rand_color())
#
# circle(2)
#
#
# screen.exitonclick()
#
# #for sides in range(3,10):
# #    draw_shape(sides)
# #    tim.pencolor(random.choice(colors))
#



