# Python_Challenge

Gitlab repo for the code generated during the Udemy Course: 100 Days of Code: The Complete Python Pro Bootcamp for 2023
(https://www.udemy.com/course/100-days-of-code/)

## License
This repo and it's code is licensed under  CC BY 4.0 

## Project status
I am currently working through the Udemy Course and will update the repo after each completed day.
The basic code for the first 15 days is not included (yet) as the course only coverd the basics of Python.
