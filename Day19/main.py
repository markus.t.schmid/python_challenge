from turtle import Turtle, Screen
import random

is_race_on = False

tim = Turtle()
screen = Screen()
screen.setup(width=500, height=400)
user_bet = screen.textinput(title="Make your bet", prompt="Which turtle will win the race? Enter a color: ")
colors = ["red", "blue", "orange", "yellow", "green", "purple"]
all_turtles = []

for turtle in range(0, len(colors)):
    new_turtle = Turtle(shape="turtle")
    new_turtle.penup()
    new_turtle.color(colors[turtle])
    new_turtle.goto(x=-240, y=150 - (turtle * 50))
    all_turtles.append(new_turtle)

if user_bet:
    is_race_on = True
print(user_bet)
while is_race_on:
    for turtle in all_turtles:
        if turtle.xcor() > 230:
            is_race_on = False
            winning_color = turtle.pencolor()
            if winning_color == user_bet:
                print(f"You have won! The {winning_color} turtle is the winner!")
            else:
                print(f"You lost. The {winning_color} turtle is the winner!")
        rand_dist = random.randint(0, 10)
        turtle.forward(rand_dist)

def move_forward():
    tim.forward(10)
def move_backward():
    tim.backward(10)
def move_right():
    tim.right(10)
def move_left():
    tim.left(10)
def clear():
    tim.clear()
    tim.penup()
    tim.home()
    tim.pendown()

screen.listen()
screen.onkey(key = "s", fun = move_backward)
screen.onkey(key = "w", fun = move_forward)
screen.onkey(key = "a", fun = move_left)
screen.onkey(key = "d", fun = move_right)
screen.onkey(key = "c", fun = clear)
screen.exitonclick()