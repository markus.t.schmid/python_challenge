from turtle import Turtle
ALIGNMENT = "center"
FONT = ('Courier', 12, 'normal')

class Score(Turtle):
    def __init__(self):
        super().__init__()
        self.penup()
        self.hideturtle()
        self.color("white")
        self.goto(0, 280)
        self.score = 0
        self.write("Score : 0", False, align=ALIGNMENT, font=FONT)

    def eat(self):
        self.clear()
        self.score += 1
        self.write(f"Score : {self.score} ", False, align=ALIGNMENT, font=FONT)

    def game_over(self):
        self.goto(0, 0)
        self.write("Game Over!", align=ALIGNMENT, font = FONT)


