from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine
gundula = CoffeeMaker()
menu = Menu()
money = MoneyMachine()
state = True
print("Hello! I am your Coffee Maker!")
while state:
    choice = input(f"What would you like? {menu.get_items()}:")
    if choice == "report":
        gundula.report()
        money.report()
    elif choice == "off":
        state = False
        print("Thank you for using Coffee Maker 2000!\nHave a nice day")
        exit()
    else:
        if gundula.is_resource_sufficient(menu.find_drink(choice)) and money.make_payment(menu.find_drink(choice).cost):
            gundula.make_coffee(menu.find_drink(choice))



